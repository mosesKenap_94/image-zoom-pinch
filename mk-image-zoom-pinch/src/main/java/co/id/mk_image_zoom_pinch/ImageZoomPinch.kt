package co.id.mk_image_zoom_pinch

import android.content.Context
import android.graphics.Matrix
import android.graphics.PointF
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import androidx.appcompat.widget.AppCompatImageView

/**
 * Author: mKenap
 * email: kvn.moseskenap@gmail.com
 * datetime: feb, 1 2021
 */

class ImageZoomPinch:AppCompatImageView, View.OnTouchListener, GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {

    //shared constructing
    private var mContext:Context? = null
    private var mScaleDetector: ScaleGestureDetector?=null
    private var mGestureDetector:GestureDetector?=null
    var mMatrix: Matrix?=null
    var mMatrixValue:FloatArray?=null
    var mode = NONE


    //Scales
    var mSaveScale = 1f
    var mMinScale = 1f
    var mMaxScale = 4f

    //view dimension
    var originWidth = 0f
    var originHeight = 0f
    var viewWidth = 0
    var viewHeight = 0
    private var mLast = PointF()
    private var mStart =  PointF()

    constructor(context: Context): super(context){
        init(context)
    }

    constructor(context: Context, attributeSet: AttributeSet?):super(context, attributeSet){
        init(context)
    }

    constructor(context: Context?, attributeSet: AttributeSet?, defStyleAttr:Int):super(context!!, attributeSet, defStyleAttr)

    private fun init(context: Context){
        super.setClickable(true)
        mContext = context
        mScaleDetector = ScaleGestureDetector(context, ScaleListener())
        mMatrix = Matrix()
        mMatrixValue = FloatArray(9)
        imageMatrix = mMatrix
        scaleType = ScaleType.MATRIX
        mGestureDetector= GestureDetector(context, this)
        setOnTouchListener(this)
    }

    private inner class ScaleListener: ScaleGestureDetector.SimpleOnScaleGestureListener(){
        override fun onScaleBegin(detector: ScaleGestureDetector?): Boolean {
            mode = ZOOM
            return true
        }

        override fun onScale(detector: ScaleGestureDetector?): Boolean {
            var mScaleFactor = detector?.scaleFactor
            val prevScale = mSaveScale

            mSaveScale *= mScaleFactor!!
            if(mSaveScale > mMaxScale){
                mSaveScale = mMaxScale
                mScaleFactor = mMaxScale/prevScale
            }else if(mSaveScale < mMinScale){
                mSaveScale = mMinScale
                mScaleFactor = mMinScale/prevScale
            }

            if(originWidth * mSaveScale <= viewWidth || originHeight * mSaveScale <=viewHeight){
                mMatrix!!.postScale(mScaleFactor, mScaleFactor, viewWidth/2.toFloat(), viewHeight/2.toFloat())
            }else{
                mMatrix!!.postScale(mScaleFactor, mScaleFactor, detector?.focusX?:0f, detector?.focusY?:0f)
            }
            fixTranslation()
            return true
        }
    }

    private fun fitToScreen(){
        mSaveScale = 1f
        val scale:Float
        val mDrawable = drawable
        if(mDrawable == null || mDrawable.intrinsicWidth == 0 || mDrawable.intrinsicHeight == 0) return
        val imageWidth = mDrawable.intrinsicWidth
        val imageHeight = mDrawable.intrinsicHeight
        val scaleX = viewWidth.toFloat() / imageWidth.toFloat()
        val scaleY = viewHeight.toFloat() / imageHeight.toFloat()
        scale = scaleX.coerceAtMost(scaleY)
        mMatrix!!.setScale(scale, scale)

        //Center the image
        var redundantYSpace = (viewHeight.toFloat() - scale * imageHeight.toFloat())
        var redundantXSpace = (viewWidth.toFloat() - scale * imageWidth.toFloat())
        redundantYSpace /= 2.toFloat()
        redundantXSpace /= 2.toFloat()
        mMatrix!!.postTranslate(redundantXSpace, redundantYSpace)
        originWidth = viewWidth - 2 * redundantXSpace
        originHeight = viewHeight - 2 * redundantYSpace
        imageMatrix = mMatrix
    }

    fun fixTranslation() {
        mMatrix!!.getValues(mMatrixValue) //put matrix values into a float array so we can analyze
        val transX = mMatrixValue!![Matrix.MTRANS_X] // get the most translation in x direction
        val transY = mMatrixValue!![Matrix.MTRANS_Y] // get the most translation in y direction
        val fitTransX = getFixTranslation(transX, viewWidth.toFloat(), originWidth * mSaveScale)
        val fitTransY = getFixTranslation(transY, viewHeight.toFloat(), originHeight * mSaveScale)
        if(fitTransX != 0f || fitTransY != 0f) mMatrix!!.postTranslate(fitTransX, fitTransY)
    }

    private fun getFixTranslation(trans:Float, viewSize:Float, contentSize:Float):Float{
        val minTrans:Float
        val maxTrans:Float

        if (contentSize <= viewSize){
            minTrans = 0f
            maxTrans = viewSize - contentSize
        }else{
            //case zoomed
            minTrans = viewSize - contentSize
            maxTrans = 0f
        }

        if(trans < minTrans ) {
            //negative x or y translation (down or to the right)
            return -trans + minTrans
        }
        if(trans > maxTrans){
            //positive x or y translation (up or the left)
            return -trans + maxTrans
        }
        return 0F
    }

    private fun getFixDragTrans(delta:Float, viewSize: Float, contentSize: Float):Float{
        return if(contentSize <= viewSize){
            0F
        }else delta
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        viewWidth = MeasureSpec.getSize(widthMeasureSpec)
        viewHeight = MeasureSpec.getSize(heightMeasureSpec)
        if(mSaveScale == 1f){

            //fit to screen
            fitToScreen()
        }

    }

    /**
     * On Touch
     */
    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        mScaleDetector!!.onTouchEvent(event)
        mGestureDetector!!.onTouchEvent(event)
        val currentPoint = PointF(event?.x?:0f, event?.y?:0f)
        when(event!!.action){
            MotionEvent.ACTION_DOWN ->{
                mLast.set(currentPoint)
                mStart.set(mLast)
                mode = DRAG
            }

            MotionEvent.ACTION_MOVE -> {
                if(mode == DRAG){
                    val dx = currentPoint.x - mLast.x
                    val dy = currentPoint.y - mLast.y
                    val fitTransX = getFixDragTrans(dx, viewWidth.toFloat(), originWidth * mSaveScale)
                    val fitTransY = getFixDragTrans(dy, viewHeight.toFloat(), originHeight * mSaveScale)
//                    Log.e("ACTION_MOVE", "dx: $dx, dy: $dy, fittransX: $fitTransX, fittransY: $fitTransY")
                    mMatrix!!.postTranslate(fitTransX, fitTransY)
                    fixTranslation()
                    mLast[currentPoint.x] = currentPoint.y
                }
            }

//            MotionEvent.ACTION_POINTER_UP -> {
//                mode = NONE
//                fitToScreen()
//            }

            MotionEvent.ACTION_UP -> {
                mode = NONE
                fitToScreen()
            }
        }
        imageMatrix  = mMatrix
        return  false
    }

    override fun onDown(e: MotionEvent?): Boolean {
        return false
    }

    override fun onShowPress(e: MotionEvent?) {}

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        return false
    }

    override fun onScroll(
        e1: MotionEvent?,
        e2: MotionEvent?,
        distanceX: Float,
        distanceY: Float
    ): Boolean {
        return false
    }

    override fun onLongPress(e: MotionEvent?) {}

    override fun onFling(
        e1: MotionEvent?,
        e2: MotionEvent?,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        return false
    }

    override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
        return false
    }

    override fun onDoubleTap(e: MotionEvent?): Boolean {
        fitToScreen()
        return false
    }

    override fun onDoubleTapEvent(e: MotionEvent?): Boolean {
        return false
    }

    companion object{

        //Images state
        const val NONE = 0
        const val DRAG = 1
        const val ZOOM = 2
    }
}